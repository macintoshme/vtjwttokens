# vtjwttokens

This is my repository for OS X and Linux JWT token scripts

mactokenglobalcopy is for use with automator:

1. Open Automator.
2. Make a new Quick Action.
3. Make sure it receives 'no input' at all programs.
4. Select Run Apple Script and type something like `do shell script "/Users/hoffmanm/Projects/vtjwttokens/mactokenglobalcopy"`.
5. Save!

Search System Preferences for Keyboard Shortcuts
You will find it under Services where you can setup a shortcut.